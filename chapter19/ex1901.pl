use strict;
use warnings;
use DBI;

my $juice = "orange juice";
my @fruits = qw/apple banana cherry fig grape/;

my $database = 'fruit.db';
my $db = "dbi:SQLite:dbname=$database";
my $dbh = DBI->connect($db);

my $create_table = <<'EOS';
create table fruit (
  juice,
  fruits
)
EOS

$dbh->do($create_table);

$dbh->do("insert into fruit (juice, fruits) values ('Orange juice', 'orange');");
$dbh->do("insert into fruit (juice, fruits) values ('Apple juice', 'apple');");

my $sth = $dbh->prepare("select * from fruit;");
$sth->execute;

while (my @row = $sth->fetchrow_array) {
  print join(', ', @row). "\n";
}

$dbh->disconnect;
