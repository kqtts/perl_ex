package BookInfo;
use strict;
use warnings;


sub new {
  my $class = shift;
  my $self = {};
  if (@_) {
    # newの第一引数をtitleに、第二引数をauthorに入れる
    $self->{title} = $_[0];
    $self->{author}  = $_[1];
    #$self->{title} = undef;
    #$self->{author} = undef;
  }
  bless  ($self, $class);
  return $self;
}

sub to_s {
  my $self =shift;
  return "$self->{title}, $self->{author}";
}

# 蔵書データを書式をつけて出力する操作を追加する
# 項目の区切り文字を引数に指定することができる
# 引数を省略した場合は改行を区切り文字にする
sub toformat {
  my $self = shift;
  my $sep  = "\n";
  return "書籍名: $self->{title}$sep著者名: $self->{author}$sep";
}

package BookInfoManager;

use strict;
use warnings;
#use DBI;
use Data::Dumper;
use base 'DBI';

sub new {

  # BookInfoManagerが入る
  my $class = shift;
  my $self = {};

  bless $self, $class;
  return $self, $class;
}

# 蔵書データの登録
sub add {
  my $self = shift;
  # データベース名
  my $database  = shift;
  my $data_source = "dbi:SQLite:dbname=$database";
  my $dbh = DBI->connect($data_source);

  my $create_table = <<'EOS';
  create table book (
  title,
  author
  )
EOS
  $dbh->do($create_table);

  print "書籍名: ";

  my $std_title = <STDIN>;
  chomp $std_title;

  print "著者名: ";
  my $std_author = <STDIN>;
  chomp $std_author;

  my $binfo = BookInfo->new($std_title, $std_author);

  $dbh->do("insert into book (title, author) values ('$binfo->{title}', '$binfo->{author}');");
}

sub listall {
  my $self = shift;
  print Dumper$self;

  #my $sth = $self->prepare("select * from book;");
}

=pod
package main;

use strict;
use warnings;
use Data::Dumper;
=cut
my $d = BookInfoManager->new;
$d->add('test.db');
$d->listall;
#print Dumper $d;
