package BookInfo;
use strict;
use warnings;

sub new {
  my $class = shift;
  my $self  = {};
  $self->{title} = shift;
  $self->{author} = shift;
  return bless $self;
}

sub to_s {
  my $self = shift;
  print "$self->{title}, $self->{author}\n";
}

my %book_infos;

$book_infos{yamada} = BookInfo->new('foo', 'bar');

$book_infos{yamada}->to_s;
