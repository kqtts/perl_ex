use strict;
use warnings;

my %friends = (
  shin => "Shin",
  shinichirou => "Shinichirou",
  shingo => "Shingo"
);

# ハッシュに要素を追加する
$friends{shinsaku} = "Shinsaku";

# ハッシュの要素を検索する(1 or 0)
print (exists $friends{shingo});
print "\n";
# ハッシュの要素を検索する(return val)
print $friends{shinsaku}."\n";
# 追加した要素を削除する
delete $friends{shinsaku};
#ハッシュの要素を検索する(見つからなくて良い)
if (exists $friends{shinsaku}){
  print $friends{shinsaku};
} else {
  print "みつかりません\n";
}
