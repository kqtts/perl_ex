use strict;
use warnings;

my $str = "Twinkle, twinkle, little star,
How I wonder what you are.
Up above the world so high,
Like a diamond in the sky.
Twinkle, twinkle, little star,
How I wonder what you are..";

my @lines = split(/\n/, $str);

# "you"にマッチする行を表示する
print "youが含まれていた行\n";
foreach (@lines) {
  if ($_ =~ /[Yy]ou/) {
    print "$_\n";
  }
}

print "\n";

# 行末に","がきている行を表示する
print "行末にカンマが含まれていた行\n";
foreach (@lines) {
  if ($_ =~ /,$/) {
    print "$_\n";
  }
}

print "\n";

# "i"がきて２文字おいて"l"がくる文字列が含まれる
print "\"i\"がきて2文字おいて\"l\"がくる文字列が含まれる行\n";
foreach (@lines) {
  if ($_ =~ /i..l/) {
    print "$_\n";
  }
}
