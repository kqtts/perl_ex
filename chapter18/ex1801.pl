package BookInfo;

use strict;
use warnings;
use Data::Dumper;

sub new {
  my $class = shift;
  my $self = {};
  if (@_) {
    # newの第一引数をtitleに、第二引数をauthorに入れる
    $self->{title} = $_[0];
    $self->{author}  = $_[1];
  }
  bless ($self, $class);
  return $self;
}

# BookInfoクラスのインスタンスをCSV形式に変換する
sub to_csv {
  my ($self, $key) = @_;

  return "$key,$self->{title},$self->{author}\n"
}

sub to_s {
  my $self =shift;
  return "$self->{title}, $self->{author}";
}

# 蔵書データを書式をつけて出力する操作を追加する
# 項目の区切り文字を引数に指定することができる
# 引数を省略した場合は改行を区切り文字にする
sub toformat {
  my $self = shift;
  my $sep  = "\n";
  return "書籍名: $self->{title}$sep著者名: $self->{author}$sep";
}

package BookInfoManager;

use strict;
use warnings;
use Data::Dumper;

sub new {
  my ($class, $self) = @_;
  $self = {};# unless defined $self;

  $self->{csv_fname} = $_[1];
  $self->{book_infos} = undef;

  bless $self, $class;
  return $self;
}

sub setup {
  my $self = shift;
  open(FH, "<$self->{csv_fname}");
  my @list = <FH>;
  for (@list) {
    my($key, $title, $author) = chomp($_
    print $_;
  }
  close(FH);
}



=pod
my $d = BookInfo->new("foo","bar");
$d->to_csv('key');
$d->to_s;
print $d->toformat;
#print Dumper $d;
=cut

my $d = BookInfoManager->new('f.txt');
$d->setup;
#print Dumper $d;
