package Student;
use strict;
use warnings;

sub new {
  my $class = shift;
  my $self  = {};
  $self->{title} = shift;
  $self->{author} = shift;
  return bless $self;
}

my %students = (
  shin => Student->new("Shin Kuboaki", 45),
  shinichirou => Student->new("Shinichirou Ooba", 35),
);

while ( my(($key, $val)) = each %students) {
  print "$key ";
  print "$val->{title} ";
  print "$val->{author}\n";
}
