package Student;
use strict;
use warnings;

sub new {
  my $class = shift;
  my $self = {};
  if (@_) {
    $self->{NAME} = shift;
    $self->{AGE}  = shift;
  }
  bless  ($self, $class);
  return $self;
}
sub to_s {
  my $self = shift;
  my $str = "$self->{name},$self->{age}\n";
  return $str;
}

package StudentBook;

sub new {
  my $class = shift;
  my $self  = {};

  $self->{shin} = Student->new('Shin', 45);
  $self->{shingo} = Student->new('Shingo',30);

  bless $self, $class;
  return $self;
}

sub printstu {
  my $self = shift;
  while ( my($key, $val) = each(%$self) ) {
    print "$key,";
    print "$val->{NAME} ";
    print "$val->{AGE}\n";
  }
}

my $d = StudentBook->new;

$d->printstu;
