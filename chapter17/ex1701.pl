use strict;
use warnings;

# 読み込みモードでオープン
open(FH,"<file.txt");
# 1行読み込み
my $line = <FH>;

close(FH);

print $line;
