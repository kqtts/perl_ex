use strict;
use warnings;

my @fruits = qw(apple banana cherry fig grape);

my $fname = "sample.txt";

open(FH, ">$fname");
for (@fruits) {
  print FH "$_\n";
}

close(FH);

open(FH, "<$fname");

my @list = <FH>;
for (@list){
  print $_;
}

close(FH);
