use strict;
use warnings;

my @indians = ("One little, two little, three little Indians\n",
            "Four little, five little, six little Indians\n",
            "Seven little, eight little, nine little Indians\n",
            "Ten little Indian boys.\n");

open(FH,">>file2.txt");
for (@indians) {
  print FH $_;
}

close(FH);
