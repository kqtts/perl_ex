use strict;
use warnings;

my @fruits = qw(apple banana cherry fig grape);

# 読み込みモードでオープン
open(OUT,">file.txt");
for (@fruits) {
  print OUT $_;
}

close(OUT);

open(IN, "<file.txt");
while (my ($line) = <IN>) {
  print $line;
}

close(IN);
