use strict;
use warnings;

# 行番号用の変数
my $line_no = 0;

open(FH,"<file.txt");
# 1行ずつ読み込む
my @list = <FH>;
for (@list){
  $line_no += 1;
  print $_;
}

close(FH);

print $line_no;
